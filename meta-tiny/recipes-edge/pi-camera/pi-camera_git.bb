# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=2b945ac999fdb1f6b17378be2ebc20f5"

SRC_URI = "git://git@gitlab.com/advt3/pi-camera.git;protocol=ssh"

# Modify these as desired

PV = "1.0+git${SRCPV}"
#SRCREV = "${AUTOREV}"
SRCREV = "7179120a461706b70e89e96ca02a337e5b1b01a5"

S = "${WORKDIR}/git"

DEPENDS = "boost opencv paho-mqtt-cpp"

inherit cmake

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE = ""

FILES_${PN} += "/usr/pi_camera/*"
