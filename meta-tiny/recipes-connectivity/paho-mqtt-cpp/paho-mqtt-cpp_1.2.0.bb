# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

SUMMARY = "MQTT CPP Client"

LICENSE = "EDL-1.0 & EPL-2.0"
LIC_FILES_CHKSUM = "file://edl-v10;md5=3adfcc70f5aeb7a44f3f9b495aa1fbf3 \
                    file://epl-v10;md5=659c8e92a40b6df1d9e3dccf5ae45a08"

SRC_URI = "git://github.com/eclipse/paho.mqtt.cpp.git;protocol=https"

SRC_URI = "git://github.com/eclipse/paho.mqtt.cpp.git;protocol=https"

# Modify these as desired
PV = "1.0.0+git${SRCPV}"
SRCREV = "33921c8b68b351828650c36816e7ecf936764379"

DEPENDS = "openssl paho-mqtt-c"
RDEPENDS_${PN} = "paho-mqtt-c"

S = "${WORKDIR}/git"

TARGET_CC_ARCH += "${LDFLAGS}"


# NOTE: unable to map the following CMake package dependencies: Doxygen Catch2 PahoMqttC
# NOTE: spec file indicates the license may be "Eclipse Distribution License 1.0 and Eclipse Public License 1.0"
inherit cmake

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE = "-DPAHO_ENABLE_TESTING=OFF -DPAHO_BUILD_STATIC=ON \
    -DPAHO_WITH_SSL=ON -DPAHO_HIGH_PERFORMANCE=ON -DPAHO_BUILD_DOCUMENTATION=FALSE -DPAHO_BUILD_SAMPLES=FALSE"