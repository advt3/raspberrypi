# Recipe created by recipetool
# This is the basis of a recipe and may need further editing in order to be fully functional.
# (Feel free to remove these comments when editing.)

SUMMARY = "MQTT C Client"
LICENSE = "EPL-2.0 | EDL-1.0"

LIC_FILES_CHKSUM = " \
    file://edl-v10;md5=3adfcc70f5aeb7a44f3f9b495aa1fbf3 \
    file://epl-v20;md5=d9fc0efef5228704e7f5b37f27192723 \
    file://notice.html;md5=943f861069889acecebf51dfa24478e2 \
    file://about.html;md5=e5662cbb5f8fd5c9faac526e4077898e \
"
SRC_URI = "git://github.com/eclipse/paho.mqtt.c;protocol=https"

# Modify these as desired
PV = "1.3.2+git${SRCPV}"
SRCREV = "64a5ff3c3b71fe019353aeacaebc66a3cf4f3461"

S = "${WORKDIR}/git"

# NOTE: unable to map the following CMake package dependencies: Doxygen
DEPENDS = "openssl"
# NOTE: spec file indicates the license may be "Eclipse Distribution License 1.0 and Eclipse Public License 2.0"
inherit cmake

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE = "-DPAHO_ENABLE_TESTING=OFF -DPAHO_BUILD_STATIC=ON \
    -DPAHO_WITH_SSL=ON -DPAHO_HIGH_PERFORMANCE=ON -DPAHO_BUILD_DOCUMENTATION=FALSE -DPAHO_BUILD_SAMPLES=FALSE"

