IMAGE_INSTALL_append = "\ 
    openssh net-tools util-linux wpa-supplicant \
    htop cmake packagegroup-core-buildessential gdb rsync \
    bash python3-pip python3-numpy pi-camera \
    "

IMAGE_FEATURES += "dev-pkgs staticdev-pkgs dbg-pkgs tools-debug tools-sdk"

IMAGE_ROOTFS_EXTRA_SPACE="1697152"

require recipes-core/images/core-image-base.bb
